---
layout: page
title: About
permalink: /about/
---

## The Team

We are a diverse team compiled out of developers and founders.

Here's a couple of us:

<div class="row">
  <div class="col s12 m4">
    <div class="author">
      <img src="../assets/img/team/James.jpg" alt="James">
      <div class="author-details">
        <div class="name">James Hiller</div>
        <div class="position">Event & Project Manager</div>
      </div>
    </div>
    <p>
      James disrupts Hamburg's home event scene with his
      startup <a href="http://b2n8.de">b2n8</a>. At ViperDev, he organizes
      events and helps passionate founders to iterate on their idea.
    </p>
  </div>
  <div class="col s12 m4">
    <div class="author">
      <img src="../assets/img/team/Hemang.jpg" alt="Hemang">
      <div class="author-details">
        <div class="name">Hemang Kumar</div>
        <div class="position">Frontend Developer</div>
      </div>
    </div>
    <p>
      Hemang codes hard in our Hamburg office.
      If an important deadline hits soon, Hemang is the hero you need - when
      we got started on apps, he wrote one in just under two hours.
    </p>
  </div>
  <div class="col s12 m4">
    <div class="author">
      <img src="../assets/img/team/Lasse.png" alt="Lasse">
      <div class="author-details">
        <div class="name">Lasse Schuirmann</div>
        <div class="position">Whatever is Needed</div>
      </div>
    </div>
    <p>
      After his computer science studies, Lasse founded
      <a href="https://viperdev.io/">ViperDev.io</a> as well as
      <a href="https://gitmate.io/">GitMate.io</a> to help startups and enterprises
      build better software.
    </p>
  </div>
</div>

If you want to meet us in person at our Hamburg office, shoot us an email at
<a href="mailto:contact@viperdev.io">contact@viperdev.io</a>.

## Our Core Values

Values are what define us as humans as well as a company. Here’s what we stand for:

### Responsive

We make your app happen within days  —  not months, not years. We respond within hours  —  not weeks. A startup needs to be fast and agile and we’re here to speed you up, not slow you down.

### Incremental

We need to be lean. We need to be fast. We need to iterate on the things that we build and do. We don't build the full product at once using the waterfall model. We learn and adapt along the way.

### Modern

Being fast and incremental is all good and well, but if we want to provide a competetive product for our clients we need to be using modern technologies.
We’re not one developer but a network of international developers and founders and we strive to keep ourselves informed about the latest and greatest stuff that’s out there.

Currently we’re hooked on Ionic which allows us building mobile apps for all platform at lightning speed and even gives us the opportunity to serve a progressive web app.

### Transparent

We come from an open source background. We love transparency and openness. As it turns out, transparency is key for good client relationships as well. You'll have a direct line our founders, developers and our network through our internal communication platform.
