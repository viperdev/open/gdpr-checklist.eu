# GDPR-Checklist.eu

Hit us up on
[mattermost](https://mattermost.gitmate.io/signup_user_complete/?id=9ecq3njoxfgmfft4uq9g35pejy) - simply
join the [~gdpr](https://mattermost.gitmate.io/viperdevpublic/channels/gdpr)
channel after signing up.

## What is This About?

We want to make it easy to comply to the GDPR. It's a good thing but oftentimes
it's hard to understand what needs to be done in order to comply.

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) is an open source website that
will analyse any webservice on GDPR compliant and provide helpful and actionable
tips.

## Who's Behind This?

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) is a shared venture between
[ViperDev.io](https://viperdev.io/) and [Commento.io](https://commento.io/).

[ViperDev.io](https://viperdev.io/) is a software development company with
the vision of enabling everyone to test out ideas with their users in no time
by building real apps with incredibly low effort.

Being compliant to GDPR is mandatory and we want to contribute to allowing
every entrepreneur to get this part of the way with as little effort as
possible.

[Commento.io](https://commento.io/) is a privacy focused discussion platform that
you can use to drive engagement for your static blog without having to worry
about speed, privacy or... building a commenting engine. Commento is currently
funded by Mozilla and has an inherent interest in protecting privacy of all
users. Making it easy for companies of all kinds to be compliant to the new
regulation helps protecting our all privacy and reduces cost and risks for all
of our users.
