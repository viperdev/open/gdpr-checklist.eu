$('#check-url').on('click', function() {
  $('#url-error').hide();
  let websiteToCheck = $('#website-to-check').val();
  if (isUrlValid(websiteToCheck)) {
    $.getJSON('https://anyorigin.com/go?url=' + websiteToCheck + '&callback=?', function(data){
      let outputOther = '';

      const pageResponse = data.contents;
      const htmlElement = document.createElement( 'html' );
      htmlElement.innerHTML = pageResponse;

      //Get all the iframes
      outputIFrames = getAllTheElements(htmlElement, 'iframe');

      //Get all the scripts
      outputScripts = getAllTheElements(htmlElement, 'script');

      //Gets products listed as meh
      // listOfNonCompliantProducts = ['disqus_identifier'];
      const disqus = pageResponse.search('disqus_identifier');
      if (disqus > 0) {
        outputOther = 'Disqus detected';
      }

      $('#gdpr-result').show();
      outputArrayContents('Scripts', 'gdpr-output-scripts', outputScripts);
      outputArrayContents('iFrames', 'gdpr-output-iframe', outputIFrames);
      outputArrayContents('Other', 'gdpr-output-other', outputOther);
    });
  } else {
    $('#url-error').show();
  }
});


function isUrlValid(userInput) {
    const res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
      return false;
    else
      return true;
}

function getAllTheElements(htmlElement, elementName) {
  let elementOutput = [];
  const elementsInPageObj = htmlElement.getElementsByTagName(elementName);
  const elementsInPageArray = Array.prototype.slice.call(elementsInPageObj);
  elementsInPageArray.forEach((element) => {
    if (elementName == 'script') {
      if (element.src != '') {
          elementOutput.push(element.src);
      }
    } else {
      elementOutput.push(element.outerHTML);
    }
  });

  return elementOutput;
}

// This function takes an array & outputs
// it to a specific div with a heading
function outputArrayContents(heading, divId, arrayToOutput) {
  let arraycontents = '';
  for (let i = 0, j = arrayToOutput.length; i < j; i++) {
    arraycontents += '<p>' + arrayToOutput[i] + '</p>';
  }
  if (arraycontents != '') {
    $('#' + divId).html(arraycontents).prepend('<h1>' + heading + '</h1>').show();
  }
}

