---
layout: post
title:  "The Beginning of GDPR-Checklist.eu"
date:   2018-04-13 10:16:21 +0200
author: "Lasse Schuirmann"
categories: venture
image:
  feature: rocket.png
  credit: "Photoroyalty — Freepik.com"
---

This blog post is a mini-FAQ about what we're building here.

## What is This About?

We want to make it easy to comply to the GDPR. It's a good thing but oftentimes
it's hard to understand what needs to be done in order to comply.

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) is an open source website that
will analyse any webservice on GDPR compliance and provide helpful and
actionable tips.

We can't give legal advise though.

## Who's Behind This?

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) is a shared venture between
[ViperDev.io](https://viperdev.io/) and [Commento.io](https://commento.io/).

[ViperDev.io](https://viperdev.io/) is a software development company with
the vision of enabling everyone to test out ideas with their users in no time
by building real apps with incredibly low effort.

Being compliant to GDPR is mandatory and we want to contribute to allowing
every entrepreneur to get this part of the way with as little effort as
possible.

[Commento.io](https://commento.io/) is a privacy focused discussion platform that
you can use to drive engagement for your static blog without having to worry
about speed, privacy or... building a commenting engine. Commento is currently
funded by Mozilla and has an inherent interest in protecting privacy of all
users. Making it easy for companies of all kinds to be compliant to the new
regulation helps protecting our all privacy and reduces cost and risks for all
of our users.

## Did You Say "Open Source"?

Yes. The code for all of this is licensed under MIT and you are free to
contribute at [GitLab](https://gitlab.com/viperdev/open/gdpr-checklist.eu).

You can join our chat at
[mattermost](https://mattermost.gitmate.io/signup_user_complete/?id=9ecq3njoxfgmfft4uq9g35pejy) - simply
join the [~gdpr](https://mattermost.gitmate.io/viperdevpublic/channels/gdpr)
channel after signing up.

## Our Core Values

Being a shared venture and an open source project, this project has it's own
values. This is what we stand for:

### Minimal

We want to be minimally invasive into our users privacy while doing our job and
we only want to solve this one problem.

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) helps companies comply with GDPR
and we do not intend to bloat it by adding other features like SEO analysis.

### Community Driven

[GDPR-Checklist.eu](https://gdpr-checklist.eu/) is an open source project and
we want to run it as a proper one.

[ViperDev.io](https://viperdev.io/) and [Commento.io](https://commento.io/) are
sponsoring hosting and the initial development of this tool. Yet we have no
intention in keeping the roadmap or any part of the development of this closed.

Anyone who wants to contribute is welcome to help.

Hit us up on
[mattermost](https://mattermost.gitmate.io/signup_user_complete/?id=9ecq3njoxfgmfft4uq9g35pejy) - simply
join the [~gdpr](https://mattermost.gitmate.io/viperdevpublic/channels/gdpr)
channel after signing up.

## What’s ahead?

We just started this. We're currently compiling some general
information and building an initial tool that pulls all `script` and `iframe`
tags from websites for analysis. Expect more to come within a week or so :)
